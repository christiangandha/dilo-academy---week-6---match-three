﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AchievementSystem : Observer
{
    public Image achievementBanner;
    public Text achievementText;

    //Event
    private TileEvent cookiesEvent, cakeEvent, candyEvent;

    private void NotifyAchievement(string key, string value)
    {
        //check jika achievement sudah diperoleh
        if (PlayerPrefs.GetInt(value) == 1)
            return;

        PlayerPrefs.SetInt(value, 1);
        achievementText.text = key + " Unlocked !";

        //pop up notifikasi
        StartCoroutine(ShowAchievementBanner());
    }
    private void ActivateAchievementBanner(bool active)
    {
        achievementBanner.gameObject.SetActive(active);
    }
    private IEnumerator ShowAchievementBanner()
    {
        ActivateAchievementBanner(true);
        yield return new WaitForSeconds(2f);
        ActivateAchievementBanner(false);
    }

    public override void OnNotify(string value)
    {
        if (GameManager.Instance.timesUpOnce == true)
            return;
        string key;

        //Seleksi event yang terjadi, dan panggil class eventnya
        if(value.Equals("Cookies event"))
        {
            cookiesEvent.OnMatch();
            if(cookiesEvent.AchievementComplete())
            {
                key = "Match first cookies";
                NotifyAchievement(key, value);
            }
        }
        if(value.Equals("Cake event"))
        {
            cakeEvent.OnMatch();
            if(cakeEvent.AchievementComplete())
            {
                key = "Match 10 cake";
                NotifyAchievement(key, value);
            }
        }
        if(value.Equals("Candy event"))
        {
            candyEvent.OnMatch();
            if(candyEvent.AchievementComplete())
            {
                key = "Match 5 candy";
                NotifyAchievement(key, value);
            }
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        PlayerPrefs.DeleteAll(); //command jika mau kesave achievement yang udh di dapatin

        //buat event
        cookiesEvent = new CookiesTileEvent(3);
        cakeEvent = new CakeTileEvent(10);
        candyEvent = new CandyTileEvent(5);

        StartCoroutine(RegisterObserverDelay());
        //foreach (var poi in FindObjectsOfType<PointOfInterest>()) //entah kenapa ini jalan terlebih dahalu sebelum objectnya di spawn, sehingga di pointofeventsnya null semua
        //{
        //    poi.RegisterObserver(this);
        //}
    }
    public IEnumerator RegisterObserverDelay()
    {
        yield return new WaitForSeconds(1f);
        foreach (var poi in FindObjectsOfType<PointOfInterest>())
        {
            poi.RegisterObserver(this);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
