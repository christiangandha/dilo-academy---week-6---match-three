﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointOfInterest : Subject
{
    //Nama dari point of interest
    [SerializeField]
    private string _poiName = "";

    //jika gameobject di disable akan menotify Observernya
    private void OnDisable()
    {
        Notify(_poiName);
    }
}
