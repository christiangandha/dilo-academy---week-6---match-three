﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CakeTileEvent : TileEvent
{
    private int currProgress = 0;
    private int maxProgress = 1;
    public CakeTileEvent(int value)
    {
        maxProgress = value;
    }

    public override bool AchievementComplete()
    {
        if (currProgress >= maxProgress)
            return true;
        else
            return false;
    }

    public override void OnMatch()
    {
        currProgress++;
    }
}
