﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grid : MonoBehaviour
{
    //Properties untuk ukuran grid
    public int gridSizeX, gridSizeY;
    //Prefab yang akan digunakan untuk background grid
    public GameObject tilePrefab;
    //Array menampung prefab ?isi?
    public GameObject[] tileFill;
    //Array 2 dimensi untuk membuat tile
    public GameObject[,] tiles;

    public Vector2 startPosition;
    public Vector2 offset;

    public GameObject BorderGridParent, TileFillParent;

    public bool GridIsMoving = false;


    // Start is called before the first frame update
    void Start()
    {
        tiles = new GameObject[gridSizeX, gridSizeY];
        CreateGrid();
    }

    private void CreateGrid()
    {
        //Menentukan offset, didapatkan dari size prefab
        offset = tilePrefab.GetComponent<SpriteRenderer>().bounds.size;
        //Menentukan posisi awal (mau di offset tile (0,0) ke bawah kiri)
        startPosition = transform.position + (Vector3.left * (offset.x * gridSizeX / 3)) + (Vector3.down * (offset.y * gridSizeY / 3));

        //Looping untuk membuat tile
        for(int i = 0; i < gridSizeX; i++)
        {
            for(int j = 0; j < gridSizeY; j++)
            {
                Vector2 pos = new Vector2(startPosition.x + (i * offset.x), startPosition.y + (j * offset.y));

                GameObject backgroundTile = Instantiate(tilePrefab, pos, tilePrefab.transform.rotation);
                backgroundTile.transform.SetParent(BorderGridParent.transform);
                backgroundTile.name = "(" + i + "," + j + ")";

                int MAX_ITERATION = 0;
                int tileFillIndex = Random.Range(0, tileFill.Length);
                while (MatchedAt(i, j, tileFill[tileFillIndex]) && MAX_ITERATION < 100)
                {
                    tileFillIndex = Random.Range(0, tileFill.Length);
                    MAX_ITERATION++;
                }
                MAX_ITERATION = 0;


                //GameObject tile = Instantiate(tileFill[tileFillIndex], pos, Quaternion.identity);
                GameObject tile = ObjectPooler.Instance.SpawnFromPool(tileFillIndex.ToString(), pos, Quaternion.identity);
                tile.transform.SetParent(TileFillParent.transform);
                //tile.name = "(" + i + "," + j + ")";
                tiles[i, j] = tile;
            }
        }

    }

    private bool MatchedAt(int column, int row, GameObject piece)
    {
        if(column > 1 && row > 1) //Cek jika ada tile yang sama dengan bawah atau sampingnya (ini dipanggil jika array tiles sudah mencapai index minimal (2,2))
        {
            if (tiles[column - 1, row].CompareTag(piece.tag) && tiles[column - 2, row].CompareTag(piece.tag))
                return true;

            if (tiles[column, row - 1].CompareTag(piece.tag) && tiles[column, row - 2].CompareTag(piece.tag))
                return true;
        } else if(column <= 1 || column <= 1) //Cek jika ada tile yang sama dengan atas atau sampingnya (ini dipanggil jika array tiles belum mencapai index (2,2), maka perlu tambahan cek apakah x/ynya udh mencapai 2 atau belum)
        {
            if(row > 1)
            {
                if (tiles[column, row - 1].CompareTag(piece.tag) && tiles[column, row - 2].CompareTag(piece.tag))
                    return true;
            }
            if(column > 1)
            {
                if (tiles[column - 1, row].CompareTag(piece.tag) && tiles[column - 2, row].CompareTag(piece.tag))
                    return true;
            }
        }

        return false;
    }

    private void DestroyMatchedAt(int column, int row)
    {
        if(tiles[column, row].GetComponent<Tile>().isMatched)
        {
            GameManager.Instance.AddScore();
            GameObject obj = tiles[column, row];
            obj.GetComponent<Tile>().isMatched = false;
            ObjectPooler.Instance.BackToPool(obj);
            //obj.SetActive(false);
            //Destroy(tiles[column, row]);
            tiles[column, row] = null;
        }
    }
    public void DestroyMatches()
    {
        for(int i = 0; i < gridSizeX; i++)
        {
            for(int j = 0; j < gridSizeY; j++)
            {
                if(tiles[i,j] != null)
                {
                    DestroyMatchedAt(i, j);
                }
            }
        }
        StartCoroutine(DecreaseRow());
    }

    private void RefillBoard() //cek looping jika ada tile yang kosong, isi lagi
    {
        for (int i = 0; i < gridSizeX; i++) 
        {
            for(int j = 0; j < gridSizeY; j++)
            {
                if (tiles[i, j] == null)
                {
                    Vector2 tempPosition = new Vector2(startPosition.x + (i * offset.x), startPosition.y + (j * offset.y));
                    int index = Random.Range(0, tileFill.Length);
                    //GameObject tileToRefill = Instantiate(tileFill[index], tempPosition, Quaternion.identity);
                    GameObject tileToRefill = ObjectPooler.Instance.SpawnFromPool(index.ToString(), tempPosition, Quaternion.identity);
                    tileToRefill.transform.SetParent(TileFillParent.transform);
                    tiles[i, j] = tileToRefill;
                }
            }
        }
    }
    private bool MatchesOnBoard()
    {
        for (int i = 0; i < gridSizeX; i++)
        {
            for (int j = 0; j < gridSizeY; j++)
            {
                if (tiles[i, j] != null)
                    if (tiles[i, j].GetComponent<Tile>().isMatched)
                    {
                        return true;
                    }
            }
        }
        return false;
    }

    private IEnumerator DecreaseRow() //Pengurangan row
    {
        int nullCount = 0;
        for (int i = 0; i < gridSizeX; i++)
        {
            for (int j = 0; j < gridSizeY; j++)
            {
                if (tiles[i, j] == null)
                    nullCount++;
                else if(nullCount > 0)
                {
                    tiles[i, j].GetComponent<Tile>().row -= nullCount;
                    tiles[i, j] = null;
                }
            }
            nullCount = 0;
        }
        yield return new WaitForSeconds(0.4f);
        StartCoroutine(FillBoard());
    }
    private IEnumerator FillBoard() //isi board kembali
    {
        GridIsMoving = true;
        RefillBoard();
        yield return new WaitForSeconds(0.5f);
        while(MatchesOnBoard())
        {
            yield return new WaitForSeconds(0.5f);
            DestroyMatches();
        }
        GridIsMoving = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
