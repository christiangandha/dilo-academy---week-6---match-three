﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    public GameObject Grid;

    [Space]
    public GameObject scoreBackground;
    private int playerScore;
    public Text scoreText;

    public int TileScore = 10;

    [Space]
    public Text timeText;
    public float maxTime = 120f;
    private float currTime;

    // Start is called before the first frame update
    void Start()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this);
        }
        else if (Instance != null)
            Destroy(gameObject);

        currTime = maxTime;
    }

    public void AddScore() //overloading
    {
        playerScore += TileScore;
        scoreText.text = playerScore.ToString();
    }

    public void AddScore(int point)
    {
        playerScore += point;
        scoreText.text = playerScore.ToString();
    }

    public bool timesUpOnce = false;
    // Update is called once per frame
    void FixedUpdate()
    {
        if (currTime >= 0)
        {
            currTime -= Time.deltaTime;
            float minutes = Mathf.CeilToInt(currTime / 60f);
            float seconds = Mathf.CeilToInt(currTime % 60f);
            timeText.text = string.Format("{0:00}:{1:00}", minutes, seconds);
        } else
        {
            if (timesUpOnce == false)
            {
                timesUpOnce = true;
                Grid.SetActive(false);
                timeText.gameObject.SetActive(false);
                scoreBackground.transform.localScale = Vector3.one * 3;
                scoreBackground.GetComponent<RectTransform>().anchoredPosition = Vector3.zero;
            }
        }

    }
}
