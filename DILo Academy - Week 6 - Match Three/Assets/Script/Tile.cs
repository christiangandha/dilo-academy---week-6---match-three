﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{
    private Vector3 firstPosition;
    private Vector3 finalPosition;
    private float swipeAngle;
    private Vector2 tempPosition;

    //menampung data dari posisi tile
    public float xPosition, yPosition;
    public int column, row;
    private Grid grid;
    private GameObject otherTile;

    public bool isMatched = false;

    //menyimpan indeks kolom dan row yang sebelumnya dipilih
    private int previousColumn, previousRow;

    private void Start()
    {
        //Menentukan posisi dari tile
        grid = FindObjectOfType<Grid>(); //Buruk, kalok bisa jgn kaya begini
        xPosition = transform.position.x;
        yPosition = transform.position.y;
        column = Mathf.RoundToInt((xPosition - grid.startPosition.x) / grid.offset.x);
        row = Mathf.RoundToInt((yPosition - grid.startPosition.y) / grid.offset.y);
    }

    private void OnMouseDown()
    {
        //mendapatkan titik awal sentuhan jari
        firstPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
    }

    private void OnMouseUp()
    {
        //mendapatkan titik akhir sentuhan jari
        if (grid.GridIsMoving == true)
            return;
        finalPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        CalculateAngle();
    }

    private void CalculateAngle()
    {
        //Menghitung sudut antara posisi awal dan posisi akhir
        swipeAngle = Mathf.Atan2(finalPosition.y - firstPosition.y, finalPosition.x - firstPosition.x) * Mathf.Rad2Deg; // * 180 / Mathf.PI;
        MoveTile();
    }

    private void MoveTile()
    {
        previousRow = row;
        previousColumn = column;
        grid.GridIsMoving = true;
        if (swipeAngle > -45 && swipeAngle <= 45) //swipe kekanan
        {
            SwipeRightMove();
        }
        else if (swipeAngle > 45 && swipeAngle <= 135) //swipe kaatas
        {
            SwipeUpMove();
        }
        else if (swipeAngle > 135 || swipeAngle <= -135) //swipe kekiri
        {
            SwipeLeftMove();
        }
        else if (swipeAngle < -45 && swipeAngle >= -135) //swipe kebawah
        {
            SwipeDownMove();
        }
    }

    //Method untuk mengatur perpindahan tile
    private void SwipeRightMove() //menukar posisi ke sebelah kanannya
    {
        if ((column + 1) > grid.gridSizeX)
            return;

        otherTile = grid.tiles[column + 1, row];
        otherTile.GetComponent<Tile>().column -= 1;
        column += 1;
    }
    private void SwipeUpMove() //menukar posisi ke sebelah atasnya
    {
        if ((row + 1) > grid.gridSizeY)
            return;

        otherTile = grid.tiles[column, row + 1];
        otherTile.GetComponent<Tile>().row -= 1;
        row += 1;
    }
    private void SwipeLeftMove() //menukar posisi ke sebelah kirinya
    {
        if ((column - 1) < 0)
            return;

        otherTile = grid.tiles[column - 1, row];
        otherTile.GetComponent<Tile>().column += 1;
        column -= 1;
    }
    private void SwipeDownMove() //menukar posisi ke sebelah kirinya
    {
        if ((row - 1) < 0)
            return;

        otherTile = grid.tiles[column, row - 1];
        otherTile.GetComponent<Tile>().row += 1;
        row -= 1;
    }

    private void Update()
    {
        CheckMatched();
        xPosition = (column * grid.offset.x) + grid.startPosition.x;
        yPosition = (row * grid.offset.y) + grid.startPosition.y;
        SwipeTile();
    }

    private void SwipeTile()
    {
        if (Mathf.Abs(xPosition - transform.position.x) > 0.1f)
        {
            //Move towards the target (animate left-right)
            tempPosition = new Vector2(xPosition, transform.position.y);
            transform.position = Vector2.Lerp(transform.position, tempPosition, 0.4f);
            StartCoroutine(checkMove());
        } else
        {
            //Directly set the position
            tempPosition = new Vector2(xPosition, transform.position.y);
            transform.position = tempPosition;
            grid.tiles[column, row] = gameObject;
        }

        if(Mathf.Abs(yPosition - transform.position.y) > 0.1f)
        {
            //Move towards the target (animate left-right)
            tempPosition = new Vector2(transform.position.x, yPosition);
            transform.position = Vector2.Lerp(transform.position, tempPosition, 0.4f);
            StartCoroutine(checkMove());
        } else
        {
            //Directly set the position
            tempPosition = new Vector2(transform.position.x, yPosition);
            transform.position = tempPosition;
            grid.tiles[column, row] = gameObject;
        }
    }

    private void CheckMatched()
    {
        if(column > 0 && column < grid.gridSizeX - 1) //Check horizontal matching (kiri-kanan)
        {
            GameObject rightTile = grid.tiles[column + 1, row];
            GameObject leftTile = grid.tiles[column - 1, row];

            if(leftTile != null && rightTile != null)
            {
                if(leftTile.CompareTag(gameObject.tag) && rightTile.CompareTag(gameObject.tag))
                {
                    isMatched = true;
                    rightTile.GetComponent<Tile>().isMatched = true;
                    leftTile.GetComponent<Tile>().isMatched = true;
                }
            }
        }
        if(row > 0 && row < grid.gridSizeY - 1) //Check vertical matching (atas-bawah)
        {
            GameObject upTile = grid.tiles[column, row + 1];
            GameObject downTile = grid.tiles[column, row - 1];
            if(upTile != null && downTile != null)
            {
                if(upTile.CompareTag(gameObject.tag) && downTile.CompareTag(gameObject.tag))
                {
                    isMatched = true;
                    upTile.GetComponent<Tile>().isMatched = true;
                    downTile.GetComponent<Tile>().isMatched = true;
                }
            }
        }

        if(isMatched)
        {
            gameObject.GetComponent<SpriteRenderer>().color = Color.grey;
        }
    }

    

    private IEnumerator checkMove()
    {
        yield return new WaitForSeconds(0.5f);

        if(otherTile != null)
        {
            if(!isMatched && !otherTile.GetComponent<Tile>().isMatched)
            {
                otherTile.GetComponent<Tile>().row = row;
                otherTile.GetComponent<Tile>().column = column;
                row = previousRow;
                column = previousColumn;
                grid.GridIsMoving = false;
            } else
            {
                grid.DestroyMatches();
            }
        }
        otherTile = null;
    }
}