﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : MonoBehaviour
{
    [System.Serializable]
    public class Pool
    {
        public string tag;
        public GameObject prefab;
        public int size;
    }

    //untuk memuat prefab pool
    public List<Pool> pools;
    public Dictionary<string, Queue<GameObject>> poolDictionary;

    //singleton
    public static ObjectPooler Instance;

    private List<GameObject> allParentTransform = new List<GameObject>();

    // Start is called before the first frame update
    void Start()
    {
        Instance = this;

        poolDictionary = new Dictionary<string, Queue<GameObject>>();

        AddIntoPool();
    }
    private void AddIntoPool()
    {
        //foreach(Pool pool in pools)
        pools.ForEach((pool) =>
        {
            Queue<GameObject> objectPool = new Queue<GameObject>();
            GameObject parent = new GameObject();
            parent.transform.SetParent(transform);
            parent.name = pool.tag + " " + pool.prefab.name;
            parent.tag = pool.prefab.tag;
            allParentTransform.Add(parent);

            int i = 0;
            while(i < pool.size)
            {
                GameObject obj = Instantiate(pool.prefab);
                obj.transform.SetParent(parent.transform);
                obj.SetActive(false);
                objectPool.Enqueue(obj);
                i++;
            }

            poolDictionary.Add(pool.tag, objectPool);
        });
    }

    //Spawn from pool digunakan untuk mengambil object dari pool
    public GameObject SpawnFromPool(string tag, Vector3 position, Quaternion rotation)
    {
        if(!poolDictionary.ContainsKey(tag))
        {
            Debug.LogWarning("Object with tag " + tag + " doesnt exist");
            return null;
        }

        GameObject objectToSpawn = poolDictionary[tag].Dequeue();
        objectToSpawn.SetActive(true);
        objectToSpawn.transform.position = position;
        objectToSpawn.transform.rotation = rotation;

        poolDictionary[tag].Enqueue(objectToSpawn);
        return objectToSpawn;
    }

    public void BackToPool(GameObject obj)
    {
        obj.SetActive(false);
        for (int i = 0; i < allParentTransform.Count; i++)
        {
            if (obj.CompareTag(allParentTransform[i].tag))
            {
                obj.transform.SetParent(allParentTransform[i].transform);
                break;
            }
        }
    }
}
